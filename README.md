# stocks-machine-learning
Using Machine Learning to Predict the future profits of stocks. 
**First look into Machine Learning with Python**

## Installation
1. Create a virtual environment | `python3 -m venv venv`
2. Activate your virtual environment | `source venv/bin/activate`
3. Run the program and follow the instructions | `python cli.py`
